#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>

#include "api.h"

void display_digit(uint8_t digit)
{
	// Cannot display bellow 0
	if (digit < 0)
	{
		digit = 0;
	}
	
	// Cannot display above 9
	if (digit > 9)
	{
		digit = 9;
	}
	
	_clear_bit(PORTPINS, _select_bit(D3));
	_clear_bit(PORTPINS, _select_bit(D2));
	_clear_bit(PORTPINS, _select_bit(D1));
	_clear_bit(PORTPINS, _select_bit(D0));
	
	if (digit > 0)
	{
		switch(digit)
		{
			case 1:
				_set_bit(PORTPINS, _select_bit(D0));
				break;
			
			case 2:
				_set_bit(PORTPINS, _select_bit(D1));
				break;
			
			case 3:
				_set_bit(PORTPINS, _select_bit(D1) | _select_bit(D0));
				break;
			
			case 4:
				_set_bit(PORTPINS, _select_bit(D2));
				break;
			
			case 5:
				_set_bit(PORTPINS, _select_bit(D2) | _select_bit(D0));
				break;
			
			case 6:
				_set_bit(PORTPINS, _select_bit(D2) | _select_bit(D1));
				break;
			
			case 7:
				_set_bit(PORTPINS, _select_bit(D2) | _select_bit(D1) | _select_bit(D0));
				break;
			
			case 8:
				_set_bit(PORTPINS, _select_bit(D3));
				break;
			
			case 9:
				_set_bit(PORTPINS, _select_bit(D3) | _select_bit(D0));
				break;
		}
		
		_delay_ms(10);
	}
}
