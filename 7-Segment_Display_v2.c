#define F_CPU 16000000UL

#define __DELAY_BACKWARD_COMPATIBLE__

#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
	uint16_t delay = 1000;
	
    DDRB |= (1 << PB1) | (1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB5); // Declare output pins
	
	PORTB |= 1 << PB1; // Set Enable pin
	
    while (1) 
    {
		PORTB &= ~((1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB5)); // Display 0: clear bits PB2, PB3, PB4, PB5 
		_delay_ms(delay);
		
		PORTB |= (1 << PB2); // Display 1
		_delay_ms(delay);
		
		PORTB &= ~((1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB5)); // Clear bits PB2, PB3, PB4, PB5
		PORTB |= (1 << PB3); // Display 2
		_delay_ms(delay);
		
		PORTB &= ~((1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB5)); // Clear bits PB2, PB3, PB4, PB5
		PORTB |= (1 << PB3) | (1 << PB2); // Display 3
		_delay_ms(delay);
		
		PORTB &= ~((1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB5)); // Clear bits PB2, PB3, PB4, PB5
		PORTB |= (1 << PB4); // Display 4
		_delay_ms(delay);
		
		PORTB &= ~((1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB5)); // Clear bits PB2, PB3, PB4, PB5
		PORTB |= (1 << PB4) | (1 << PB2); // Display 5
		_delay_ms(delay);
		
		PORTB &= ~((1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB5)); // Clear bits PB2, PB3, PB4, PB5
		PORTB |= (1 << PB4) | (1 << PB3); // Display 6
		_delay_ms(delay);
		
		PORTB &= ~((1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB5)); // Clear bits PB2, PB3, PB4, PB5
		PORTB |= (1 << PB4) | (1 << PB3) | (1 << PB2); // Display 7
		_delay_ms(delay);
		
		PORTB &= ~((1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB5)); // Clear bits PB2, PB3, PB4, PB5
		PORTB |= (1 << PB5); // Display 8
		_delay_ms(delay);
		
		PORTB &= ~((1 << PB2) | (1 << PB3) | (1 << PB4) | (1 << PB5)); // Clear bits PB2, PB3, PB4, PB5
		PORTB |= (1 << PB5) | (1 << PB2); // Display 9
		_delay_ms(delay);
    }
}
