#define F_CPU 16000000UL

#define __DELAY_BACKWARD_COMPATIBLE__

#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
	uint16_t delay = 1000;
	
    DDRB = 0b00111110;
	
    while (1) 
    {
		PORTB = 0b00000010; // 0 + EN
		_delay_ms(delay);
		
		PORTB = 0b00000110; // 1 + EN
		_delay_ms(delay);
		
		PORTB = 0b00001010; // 2 + EN
		_delay_ms(delay);
		
		PORTB = 0b00001110; // 3 + EN
		_delay_ms(delay);
		
		PORTB = 0b00010010; // 4 + EN
		_delay_ms(delay);
		
		PORTB = 0b00010110; // 5 + EN
		_delay_ms(delay);
		
		PORTB = 0b00011010; // 6 + EN
		_delay_ms(delay);
		
		PORTB = 0b00011110; // 7 + EN
		_delay_ms(delay);
		
		PORTB = 0b00100010; // 8 + EN
		_delay_ms(delay);
		
		PORTB = 0b00100110; // 9 + EN
		_delay_ms(delay);
    }
}

