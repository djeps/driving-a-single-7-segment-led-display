#define F_CPU 16000000UL

#define __DELAY_BACKWARD_COMPATIBLE__

#include <avr/io.h>
#include <util/delay.h>

#define bit_select(bit)       ( (1) << (bit) )
#define bit_set(val, mask)    ( (val) |= (mask) )
#define bit_clear(val, mask)  ( (val) &= (~mask) )
#define bit_toggle(val, mask) ( (val) ^= (mask) )
#define bit_read(val, mask)   ( (val) & (mask) )

#define OUTPORT  DDRB
#define PORTPINS PORTB

#define EN_PIN   PB1

#define D0       PB2
#define D1       PB3
#define D2       PB4
#define D3       PB5

#define enable_output()   bit_set( (OUTPORT), ( bit_select(EN_PIN) | bit_select(D0) | bit_select(D1) | bit_select(D2) | bit_select(D3) ) );
#define enable_display()  bit_set( (PORTPINS), ( bit_select(EN_PIN) ) )
#define disable_display() bit_clear( (PORTPINS), ( bit_select(EN_PIN) ) )

#define set_display()     enable_display()
#define clear_display()   disable_display()

void display_digit(uint8_t digit);

int main(void)
{
	uint16_t delay = 1000;
	
	enable_output();
	enable_display();
	
    while (1) 
    {
		clear_display();
		_delay_ms(delay);
		
		set_display();
		
		display_digit(0); 
		_delay_ms(delay);
		
		display_digit(1);
		_delay_ms(delay);
		
		display_digit(2);
		_delay_ms(delay);
		
		display_digit(3);
		_delay_ms(delay);
		
		display_digit(4);
		_delay_ms(delay);
		
		display_digit(5);
		_delay_ms(delay);
		
		display_digit(6);
		_delay_ms(delay);
		
		display_digit(7);
		_delay_ms(delay);
		
		display_digit(8);
		_delay_ms(delay);
		
		display_digit(9);
		_delay_ms(delay);
    }
}

void display_digit(uint8_t digit)
{
	// Cannot display bellow 0
	if (digit < 0)
	{
		digit = 0;
	}
	
	// Cannot display above 9
	if (digit > 9)
	{
		digit = 9;
	}
	
	bit_clear(PORTPINS, bit_select(D3));
	bit_clear(PORTPINS, bit_select(D2));
	bit_clear(PORTPINS, bit_select(D1));
	bit_clear(PORTPINS, bit_select(D0));
	
	if (digit > 0)
	{
		switch(digit)
		{
			case 1:
				bit_set(PORTPINS, bit_select(D0));
				break;
			
			case 2:
				bit_set(PORTPINS, bit_select(D1));
				break;
			
			case 3:
				bit_set(PORTPINS, bit_select(D1) | bit_select(D0));
				break;
			
			case 4:
				bit_set(PORTPINS, bit_select(D2));
				break;
				
			case 5:
				bit_set(PORTPINS, bit_select(D2) | bit_select(D0));
				break;
			
			case 6:
				bit_set(PORTPINS, bit_select(D2) | bit_select(D1));
				break;
			
			case 7:
				bit_set(PORTPINS, bit_select(D2) | bit_select(D1) | bit_select(D0));
				break;
			
			case 8:
				bit_set(PORTPINS, bit_select(D3));
				break;
			
			case 9:
				bit_set(PORTPINS, bit_select(D3) | bit_select(D0));
				break;
		}		
	}
}
