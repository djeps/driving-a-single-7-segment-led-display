#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>

#include "api.h"

void display_digit(digit_t digit)
{
	_clear_bit(PORTPINS, _select_bit(D3));
	_clear_bit(PORTPINS, _select_bit(D2));
	_clear_bit(PORTPINS, _select_bit(D1));
	_clear_bit(PORTPINS, _select_bit(D0));
	
	if (digit != ZERO)
	{
		switch(digit)
		{
			case ONE:
				_set_bit(PORTPINS, _select_bit(D0));
				break;
			
			case TWO:
				_set_bit(PORTPINS, _select_bit(D1));
				break;
			
			case THREE:
				_set_bit(PORTPINS, _select_bit(D1) | _select_bit(D0));
				break;
			
			case FOUR:
				_set_bit(PORTPINS, _select_bit(D2));
				break;
			
			case FIVE:
				_set_bit(PORTPINS, _select_bit(D2) | _select_bit(D0));
				break;
			
			case SIX:
				_set_bit(PORTPINS, _select_bit(D2) | _select_bit(D1));
				break;
			
			case SEVEN:
				_set_bit(PORTPINS, _select_bit(D2) | _select_bit(D1) | _select_bit(D0));
				break;
			
			case EIGHT:
				_set_bit(PORTPINS, _select_bit(D3));
				break;
			
			case NINE:
				_set_bit(PORTPINS, _select_bit(D3) | _select_bit(D0));
				break;
			
			case ZERO:
			default:
				break;
		}
		
		_delay_ms(10);
	}
}
