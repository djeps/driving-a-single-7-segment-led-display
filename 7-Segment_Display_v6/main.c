#define F_CPU 16000000UL

#define __DELAY_BACKWARD_COMPATIBLE__

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "api.h"

uint16_t timer = 0;
uint8_t longPress = FALSE;

int main(void)
{ 
	uint8_t manual = FALSE;
	
	uint16_t delay = 1000;
	
	uint8_t cnt = 0;
	
	_set_outputs_port_b();
	_set_inputs_port_c();
	
	TCCR0A |= 1 << WGM01;  // Timer 0 mode 2 - CTC
	TCCR0B |= 1 << CS02;   // Set pre-scaler to 256
	OCR0A = 125;           // Number of ticks in Output Compare Register for a delay of 2ms
	TIMSK0 |= 1 << OCIE1A; // Trigger interrupt when counter(TCNT0) >= OCR0A
	
	sei();
	
	if (_switch_pressed())
	{
		manual = TRUE;
	}
	
    while (1) 
    {					
		if (manual)
		{					
			if (_switch_pressed())
			{
				_enable_display();
				
				if (!longPress)
				{
					cnt++;
					
					display_digit(cnt);
				}
				
				if (cnt == 9)
				{
					cnt = 0;
				}
			}
			else
			{
				_disable_display();
				timer = 0; // Reset the timer
			}
		}
		else
		{
			_enable_display();
						
			display_digit(1);
			_delay_ms(delay);
		
			display_digit(2);
			_delay_ms(delay);
		
			display_digit(3);
			_delay_ms(delay);
		
			display_digit(4);
			_delay_ms(delay);
		
			display_digit(5);
			_delay_ms(delay);
		
			display_digit(6);
			_delay_ms(delay);
		
			display_digit(7);
			_delay_ms(delay);
		
			display_digit(8);
			_delay_ms(delay);
		
			display_digit(9);
			_delay_ms(delay);
		}
    }
	
	return 0;
}

ISR(TIMER0_COMPA_vect)
{
	timer++;
	
	if (timer > 1) {
		longPress = TRUE; // A button press > 2ms
	}
	else
	{
		longPress = FALSE;
	}
}

