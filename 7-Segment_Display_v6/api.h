#ifndef API_H_
#define API_H_

#include "hal.h"

// Refactor to use bit fields instead
#define TRUE     0x1
#define FALSE    0x0

#define EN_PIN   PB1
#define SW       PC2

#define D0       PB2
#define D1       PB3
#define D2       PB4
#define D3       PB5

#define PORTPINS PORTB

#define _set_outputs_port_b()            _set_bit( ( DDRB ), ( _select_bit(EN_PIN) | _select_bit(D0) | _select_bit(D1) | _select_bit(D2) | _select_bit(D3) ) )
#define _set_inputs_port_c()             _clear_bit( ( DDRC ), ( _select_bit(SW) ) )
#define _enable_internal_pullup_port_c() _set_bit( (PORTC), ( _select_bit(SW) ) )

#define _enable_display()                _set_bit( ( PORTB ), ( _select_bit( EN_PIN ) ) )
#define _disable_display()               _clear_bit( ( PORTB ), ( _select_bit( EN_PIN ) ) )

#define _set_display()                   _enable_display()
#define _clear_display()                 _disable_display()

#define _switch_pressed()                _read_bit(PINC, _select_bit(SW))

enum digits
{
	ZERO,
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE
};

typedef enum digits digit_t;

void display_digit(digit_t digit);
void resetApiTimer(uint16_t *pTimer);

#endif /* API_H_ */